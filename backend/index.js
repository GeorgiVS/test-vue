const express = require("express");
const data = require("./data/data.json");
const names = require("./data/names.json");
const app = express();

app.use(function (_req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/data", function (req, res, next) {
  res.send(data);
});

app.get("/names", function (req, res, next) {
  res.send(names);
});

app.listen(5000, () => console.warn("Server is running on port 5000"));
