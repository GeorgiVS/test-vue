export interface IProduct {
	id: string;
	name: string;
	priceUsd: number;
	oldPriceUsd: number | undefined;
	getPriceRub: () => number;
	stock: number;
}
