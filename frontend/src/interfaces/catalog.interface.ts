import type { IProduct } from "./product.interface";

export interface ICatalog {
	name: string;
	id: string;
	products: IProduct[];
}
