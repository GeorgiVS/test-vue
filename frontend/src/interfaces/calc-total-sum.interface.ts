export interface ICalcTotalSum {
	id: string;
	amount: number;
}
