import type { IProduct } from "@/interfaces";

export function checkChangePrice(product: IProduct) {
	let className = "";
	if (product.oldPriceUsd && product.oldPriceUsd !== product.priceUsd) {
		className =
			product.priceUsd > product.oldPriceUsd ? "price-up" : "price-down";
	}
	return className;
}
