import { defineStore } from "pinia";

export const useExchangeRatesStore = defineStore({
	id: "exchangeRates",
	state: () => ({
		usd: {
			current: 20,
			old: 20,
		},
	}),
});
