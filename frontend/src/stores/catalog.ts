import type { ICatalog, IProduct } from "@/interfaces";
import { defineStore } from "pinia";
import { useExchangeRatesStore } from "./exchange-rates";
import { useCartStore } from "./cart";
import { getStringHash } from "@/functions";

interface IState {
	oldCatalog: ICatalog[];
	catalog: ICatalog[];

	//так не хорошо, но все же это боевой проект
	oldNames: any;
	oldData: any;
}

const API_URL = "http://localhost:5000"; //В .env по хорошему перенести

export const useCatalogStore = defineStore({
	id: "catalog",
	state: (): IState => ({
		oldNames: [],
		oldData: [],
		oldCatalog: [],
		catalog: [],
	}),
	actions: {
		async getCatalog() {
			const catalog: ICatalog[] = [];
			const exchangeRates = useExchangeRatesStore();
			const results = await Promise.all([
				fetch(`${API_URL}/data`),
				fetch(`${API_URL}/names`),
			]);
			const {
				Value: { Goods: data },
			} = await results[0].json();
			const names = await results[1].json();

			//Если данные от сервера не изменились, не будем снова собирать каталог для фронта
			if (
				getStringHash(this.oldNames) === getStringHash(JSON.stringify(names)) &&
				getStringHash(this.oldData) === getStringHash(JSON.stringify(data))
			) {
				return;
			}
			this.oldNames = JSON.stringify(names);
			this.oldData = JSON.stringify(data);
			this.oldCatalog = this.catalog;

			//Соберем каталог так, чтобы было удобно работать с ним на фронте
			for (let key in names) {
				let products: IProduct[] = [];

				const dataGroup = data.filter((item: { G: string }) => item.G == key);

				if (!dataGroup.length) continue;

				const oldCateogry = this.oldCatalog.find(
					(item) => item.id == dataGroup[0].G
				);

				for (let productKey in names[key].B) {
					dataGroup.forEach((obj: { T: string; C: number; P: number }) => {
						if (productKey == obj.T) {
							products.push({
								name: names[key].B[productKey].N,
								id: obj.T,
								priceUsd: obj.C,
								oldPriceUsd: oldCateogry?.products.find(
									(item) => item.id == obj.T
								)?.priceUsd,
								getPriceRub() {
									return Math.ceil(obj.C * exchangeRates.usd.current);
								},
								stock: obj.P,
							});
						}
					});
				}
				catalog.push({
					name: names[key].G,
					id: dataGroup[0].G,
					products,
				});
			}
			this.catalog = catalog;
			useCartStore().sync(this.catalog);
		},
	},
});
