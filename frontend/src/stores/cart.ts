import { defineStore } from "pinia";
import type { ICatalog, IProduct } from "@/interfaces";

interface IState {
	cart: Set<IProduct>;
	totalSum: number;
	sync: (catalog: ICatalog[]) => void;
	calcTotalSum: (id?: string, amount?: number) => void;
}

export const useCartStore = defineStore({
	id: "cart",
	state: (): IState => ({
		cart: new Set(),
		totalSum: 0,

		//Синхронизируем товар в корзине с товаром в новом каталоге. Если он изменился - актуализируем, если больше не доступен - удалим
		sync(catalog) {
			const arr: IProduct[] = [];

			this.cart.forEach((item) => {
				catalog.forEach((category) => {
					const checkProduct = category.products.find(
						(product: IProduct) => product.id === item.id
					);
					if (checkProduct) {
						arr.push(checkProduct);
					}
				});
			});
			console.dir(arr);
			this.cart = new Set(arr);
		},

		calcTotalSum(id?: string, amount?: number) {
			this.totalSum = [...this.cart].reduce((acc, item) => {
				if (id === item.id && amount) {
					acc += item.getPriceRub() * amount;
				} else {
					acc += item.getPriceRub();
				}

				return acc;
			}, 0);
		},
	}),
});
