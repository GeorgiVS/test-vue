import type { ICatalog, IProduct } from "@/interfaces";
import { defineStore } from "pinia";
import { useExchangeRatesStore } from "./exchange-rates";
import { useCartStore } from "./cart";
import { getStringHash } from "@/functions";

interface IState {
	oldCatalog: ICatalog[];
	catalog: ICatalog[];

	//так не хорошо, но все же это боевой проект
	oldNames: any;
	oldData: any;
}

const API_URL = "http://localhost:5000"; //В .env по хорошему перенести

export const useCatalogStore = defineStore({
	id: "catalog",
	state: (): IState => ({
		oldNames: [],
		oldData: [],
		oldCatalog: [],
		catalog: [],
	}),
	actions: {
		async getCatalog() {
			const exchangeRates = useExchangeRatesStore();
			const results = await Promise.all([
				fetch(`${API_URL}/data`),
				fetch(`${API_URL}/names`),
			]);
			const {
				Value: { Goods: data },
			} = await results[0].json();
			const names = await results[1].json();

			//Если данные от сервера не изменились, не будем снова собирать каталог для фронта
			if (
				getStringHash(this.oldNames) === getStringHash(JSON.stringify(names)) &&
				getStringHash(this.oldData) === getStringHash(JSON.stringify(data))
			) {
				return;
			}
			this.oldNames = JSON.stringify(names);
			this.oldData = JSON.stringify(data);
			this.oldCatalog = this.catalog;

			//Соберем каталог так, чтобы было удобно работать с ними на фронте.
			const namesFlatList = Object.entries(names).reduce(
				(sum: Object, [index, item]) => ({
					...sum,
					...Object.entries(item.B).reduce(
						(sum: Object, [index, item]) => ({
							...sum,
							[index]: item.N,
						}),
						{}
					),
				}),
				{}
			);

			const categories: any = Object.entries(names).reduce(
				(sum, [index, item]) => [...sum, index],
				[]
			);

			const itemsFlatList = categories
				.reduce(
					(sum, categoryId) => [
						...sum,
						...data.filter((item: { G: string }) => item.G == categoryId),
					],
					[]
				)
				.filter((item) => !!item);

			const items = itemsFlatList.reduce(
				(sum, obj: { T: string; C: number; P: number }) => [
					...sum,
					{
						priceUsd: obj.C,
						oldPriceUsd: JSON.parse(this.oldData).find(
							(item) => item.G == obj.G && item.T == obj.T
						)?.C,
						getPriceRub() {
							return Math.ceil(obj.C * exchangeRates.usd.current);
						},
						stock: obj.P,
						id: obj.T,
						categoryId: +obj.G,
						name: namesFlatList[obj.T],
					},
				],
				[]
			);

			const catalog: ICatalog[] = Object.entries(names)
				.reduce(
					(sum, [key, item]) => [
						...sum,
						{
							name: item.G,
							id: +key,
							products: items.filter(
								(itemToGet) => itemToGet.categoryId === +key
							),
						},
					],
					[]
				)
				.filter((item) => item.products.length);

			this.catalog = catalog;
			useCartStore().sync(this.catalog);
		},
	},
});
